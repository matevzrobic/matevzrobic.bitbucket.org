var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
  function generiraj() {
    generirajPodatke(1);
    generirajPodatke(2);
    generirajPodatke(3);
}
 
function generirajPodatke(stPacienta) {
    
    var sessionId = getSessionId();

    var ime;
    var priimek;
    var datumRojstva;
    var teza;
    var visina;
    
    switch(stPacienta) {
        case 1:
            ime ="John";
            priimek="Wick";
            datumRojstva="1981-05-10";
            teza="90";
            visina="180";
            break;
        
        case 2:
            ime ="Lionel";
            priimek="Messi";
            datumRojstva="1981-03-20";
            teza="80";
            visina="160";
            break;
        case 3:
            ime ="Katarina";
            priimek="Vrabec";
            datumRojstva="1991-12-12";
            teza="63";
            visina="166";
            break;
            
        
    }
    
    $.ajaxSetup({
        headers: {"Ehr-Session": sessionId}
    });
    
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        success: function (data) {
            var ehrId = data.ehrId;
            var partyData = {
                firstNames: ime,
                lastNames: priimek,
                dateOfBirth: datumRojstva,
                partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
            };
            
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    
                    if (party.action == 'CREATE') {
                        
                        if(stPacienta == 1){
                            document.getElementById('pacienti').options[1].value = ehrId;
                        }else
                        if(stPacienta == 2){
                            document.getElementById('pacienti').options[2].value = ehrId;
                        }else
                        if(stPacienta == 3){
                            document.getElementById('pacienti').options[3].value = ehrId;
                        }
                        
                        dodajPodatke(sessionId, ehrId);
                        
                    }
                },
                error: function(err) {
                    if (err) {
                        console.log(err);
                        alert("Napaka!");
                    }
                }
            });
        }
    });

  return ehrId;
}

function dodajPodatke(pacient, er){
    sessionId = getSessionId();
    
     $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	var compostitionData = {
	    "ctx/language": "en",
	    "ctx/territory": "SI",
	    "vital_signs/height_length/any_event/body_height_length": visina;
	    "vital_signs/body_weight/any_event/body_weight": teza,
	};
	var queryParams = {
	    ehrId: ehrId,
	    templateId: 'Vital Signs',
	    format: 'FLAT',
	    committer: "Matevz Robic"
	};
	$.ajax({
	    url: baseUrl + "/composition?" + $.param(queryParams),
	    type: 'POST',
	    contentType: 'application/json',
	    data: JSON.stringify(compostitionData),
	    success: function (res) {
	      $("#napaka").html("<p>Generiranje Pacientov Uspešno!</p>");
	    },
	    error: function(err) {
	        $("#napaka").html("<p>Napaka!</p>");
	    }
	});
    
    var ehrId = er;
    
}

function izpisiPodatke(){
    ehrId = document.querySelector("#ehrPacienta").value;
    sessionId = getSessionId();
    $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (data) {
        var ime = data.party.firstNames;
        var priimek = data.party.lastNames;
        var datum= data.party.dateOfBirth;
        var vmes = data.party.dateOfBirth.split("-");
        var starost = 2018 - vmes[0];
        console.log("izp");
        document.querySelector("#imePacienta").value = ime;
        document.querySelector("#primekPacienta").value = priimek;
        document.querySelector("#datumRojstvaPacienta").value = datum;
        document.querySelector("#starostPacienta").value = starost;
        document.querySelector("#starmet").value = starost;
        bmr();
        $("#napaka").html("<p>Izpis Podatkov Uspešen!</p>");
    },
    error: function(neki){
        $("#napaka").html("<p>Prosimo, preverite pravilnost vnosnega polja!</p>");
    }
});

function indeks(){
    var teza= document.querySelector("#teza").value;
    var visina = document.querySelector("#visina").value;
    var index = teza/((visina*visina)/10000);
    document.querySelector("#indeks").value = index;
}

function meta() {
    var teza = document.querySelector("#tezam").value;
    var visina = document.querySelector("#visinam").value;
    var starost = document.querySelector("#starostm").value;
    var meta =(13.7 * teza) + (5 * visina) - (6.8 * starost) + 66;
    document.querySelector("#indeksm").value = meta;
    
    
}

$.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    success: function (res) {
        document.querySelector("#tezaPacienta").value = res[0].weight;
        document.querySelector("#teza").value = res[0].weight;
        document.querySelector("#tezam").value = res[0].weight;
    }
});

$.ajax({
    url: baseUrl + "/view/" + ehrId + "/height",
    type: 'GET',
    success: function (res) {
        document.querySelector("#visinaPacienta").value = res[0].height;
        document.querySelector("#visina").value = res[0].height;
        document.querySelector("#visinam").value = res[0].height;
        meta();
    }
});

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
